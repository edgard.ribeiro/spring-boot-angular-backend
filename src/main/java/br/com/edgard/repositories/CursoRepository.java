package br.com.edgard.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.edgard.modelo.Curso;

public interface CursoRepository extends JpaRepository<Curso, Long> {
	Curso findByNome(String nome);
}
